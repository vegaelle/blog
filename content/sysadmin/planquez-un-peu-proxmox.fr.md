Title: Planquez (un peu) Proxmox
Permalink: planquez-un-peu-proxmox
Date: 2014-07-14
Modified: 2014-07-14
Tags: proxmox, virtualisation, sécurité, ssh
Category: Sysadmin

[Proxmox](https://www.proxmox.com/) est un outil intéressant, simple à prendre
en main, offrant le moyen de gérer un serveur de virtualisation. Je l’utilise
depuis pas mal de temps, avec des VM
[qemu-kvm](http://www.linux-kvm.org/page/Main_Page) comme des conteneurs
[openvz](https://openvz.org/Main_Page).

<!-- cut -->

Concrètement, il s’agit d’une distribution GNU/Linux basée sur
[Debian](https://debian.org) embarquant les solutions de
virtualisation/conteneurs suscitées, et une jolie interface web s’appuyant sur
la lib JS [ExtJS](http://www.sencha.com/products/extjs/) pour faciliter
l’administration de tout ça. Le problème, c’est que cette interface est
publique, accessible simplement via une URL, et il est possible de se connecter
en root directement dessus (via un module [pam](http://www.linux-pam.org/)).
Or, je ne souhaite pas que l’on puisse avoir la main totale sur un serveur avec
un simple mot de passe, fut-il d’une robustesse redoutable. Une authentification
par mot de passe, c’est trop faible.

### Apache

Du coup, ma solution est de reconfigurer le serveur web de Proxmox afin de
passer par un tunnel SSH en tant que seul moyen d’accéder à la console. Le
serveur web en question est un Apache, et il y a 2 fichiers à modifier pour
sécuriser cette partie : pour les trouver, il suffit de rechercher les
occurrences de « listen » dans le dossier d’Apache :

    :::console
    # grep -Rl Listen /etc/apache2/
    /etc/apache2/sites-enabled/pve.conf
    /etc/apache2/sites-available/pve.conf
    /etc/apache2/ports.conf

Le fichier */etc/apache2/sites-enabled/pve.conf* est une stricte copie de celui
dans sites-available, donc la modification sera à répliquer dans les deux
fichiers.

C’est la directive « *Listen* » d’Apache qui nous intéresse. On en trouve 2 dans
ports.conf, et un dans pve.conf. Elle indique simplement sur quelle interface on
désire faire écouter Apache. Si on précise seulement le port, on écoute sur
toutes les interfaces. Mais il est heureusement possible de préciser l’adresse
sur laquelle on souhaite écouter via « *Listen &lt;address&gt;:&lt;port&gt;* ».
Concrètement, on va changer ces fichiers pour n’écouter que sur l’adresse locale
(*127.0.0.1*).

    :::apache
    Listen 127.0.0.1:80

N’oubliez pas de remplacer le port par celui de la ligne en question. Redémarrez
le serveur (« *service apache2 restart* ») et constatez que l’interface web
n’est plus accessible via son adresse publique.

### SSH

Si Apache n’écoute plus que sur l’adresse *localhost*, il vous faut être **sur**
cette machine pour accéder à l’interface. Mais je doute que
[lynx](http://lynx.browser.org/) se comporte très bien avec une interface web
pleine de JS, alors il nous faut une autre solution : le tunnel SSH, dont je
parlais plus haut. Le principe est de se servir du serveur en tant que proxy
encapsulé dans du SSH (ainsi, pas besoin d’ajouter un *daemon* sur la machine).
La requête HTTP telle que vous la tapez sera donc réellement lancée depuis le
point de sortie (le serveur). Et si vous faites pointer votre navigateur sur
l’adresse *127.0.0.1*, c’est bel et bien sur le serveur que vous vous
retrouverez.

Mais pour que cette solution soit un tant soit peu efficace, il faut forcer
l’utilisation de clés SSH pour se connecter au serveur, à cause de la
problématique évoquée plus haut. Ma solution perso est de n’autoriser la
connexion SSH qu’avec une clé, et uniquement sur un utilisateur n’ayant aucun
droit, si ce n’est celui d’utiliser *su* pour passer en root. Il faudra donc
posséder la bonne clé SSH (et sa *passphrase*) ainsi que le pass root pour
pouvoir se connecter au serveur. Avant de modifier la configuration du serveur,
il vaut mieux installer la clé SSH. Si vous n’en avez pas sur votre poste,
créez-en une (à taper sur votre poste local) :

    :::console
    $ ssh-keygen

Je vous laisse le soin de choisir les options de votre clé. Par principe,
utilisez le maximum de bits possibles selon l’algorithme choisi. Choisissez
soigneusement votre *passphrase*, de sorte à la retenir sans négliger sa
robustesse.

Il nous faut maintenant créer un utilisateur sur le serveur, sur lequel on se
connectera :

    :::console
    # useradd -m poney # choisissez un nom pas trop évident
    # passwd poney # choisissez un mot de passe temporaire

Nous spécifions un mot de passe pour cet utilisateur, car il faudra s’y
connecter une première fois pour y stocker la clé. On supprimera le mot de passe
tout à l’heure.

Ensuite, utilisez *ssh-copy-id* pour ajouter votre clé sur le serveur
(toujours à taper sur votre poste local) :

    :::console
    $ ssh-copy-id poney@<adresse du serveur>

Si besoin, spécifiez via l’option *-i* le fichier de clé à envoyer au serveur.
Le mot de passe du compte *poney* vous est demandé.

Ensuite, nous sommes prêts à modifier la configuration du serveur. Voici donc
les lignes à modifier dans /etc/ssh/sshd_config :

    Port 9347 # on modifie le port par défaut, pour éviter les scanners idiots, choisissez-en un inutilisé au hasard
    PermitRootLogin no
    PasswordAuthentication no # on ne permet pas la connexion par un mot de passe

Redémarrez le serveur via *service ssh restart*, **ne vous déconnectez surtout
pas**, au cas où votre configuration ait rendu toute nouvelle connexion
impossible, puis tentez de vous connecter depuis votre utilisateur fraîchement
créé (par le biais de sa clé) :

    :::console
    $ ssh -p 9347 poney@<adresse du serveur>

Si tout se passe bien, ce ne sera plus le mot de passe du compte *poney* qui
sera demandé, mais la *passphrase* de votre clé SSH (possiblement dans une
fenêtre graphique, selon votre configuration). Si vous accédez au shell de
*poney* après ça, c’est que tout est bon. Vous pouvez maintenant accéder au root
en tapant « su - » (le tiret final permet de réinitialiser les variables
d’environnement, pour éviter que d’éventuelles saloperies ayant infecté *poney*
ne soient transmises à *root*), suivi de votre mot de passe *root*.

Respirez, on est presque à la fin.

Maintenant que l’on sait se connecter au serveur via un utilisateur tiers et via
une clé SSH uniquement, on peut supprimer le mot de passe de cet utilisateur :

    :::console
    # passwd -d poney

La partie serveur est maintenant un peu plus sécurisée qu’avant. Il reste une
dernière chose à faire pour pouvoir accéder de nouveau à l’interface web.

### Poste client

Avant d’oublier, nous allons utiliser le fichier de configuration
*~/.ssh/config* sur notre poste client pour faciliter la syntaxe de la
connexion :

    Host mon-serveur # remplacez par le nom que vous souhaitez
    HostName <ip du serveur>
    User poney
    Port 9347

Ceci vous permettra de vous connecter par cette simple commande :

    :::console
    $ ssh mon-serveur

Maintenant, nous allons enfin configurer le tunnel SSH. Rien de bien complexe,
c’est intégré dans OpenSSH :

    :::console
    $ ssh -ND 1234 mon-serveur # utilisez un port de votre choix

La connexion SSH se fait, et un proxy SOCKS5 est mis en place sur le port 1234
de votre adresse locale. Celui-ci sort sur le serveur. Il suffit alors de
configurer votre navigateur pour utiliser ce proxy. Sous Firefox, par exemple,
la configuration se trouve dans *Édition → Préférences → Avancé → Réseau →
Connexion → Paramètres*. Renseignez « 127.0.0.1 » comme hôte SOCKS, en mode
SOCKS v5, avec le port choisi plus haut. Surtout, supprimez les exceptions dans
le champ du dessous, car sinon vous ne pourrez pas accéder à l’interface Proxmox
(étant donné qu’elle sera sur l’adresse 127.0.0.1).

<p><img src="/images/proxmox/firefox_tunnel_ssh.png" alt="Configuration du proxy
dans Firefox" title="Configuration du proxy dans Firefox"
class="aligncenter size-full" /></p>

Maintenant, faites pointer votre navigateur sur l’adresse *127.0.0.1* Si tout va
bien, vous serez redirigé sur le port 8006 en SSL, car c’est la configuration
par défaut de Proxmox, et vous pourrez accéder à l’interface comme avant.

Lorsque vous aurez fini ce que vous aviez à faire sur l’interface, reconfigurez
votre navigateur pour utiliser votre ancienne configuration de proxy,
déconnectez votre tunnel SSH (en tapant simplement « exit » ou en appuyant sur
*ctrl-D*). Si vous voulez y accéder de nouveau :

1. lancez le tunnel SSH (*ssh -ND 1234 nom-serveur*)
2. configurez les paramètres de proxy de votre navigateur
3. visitez http://127.0.0.1 dans votre navigateur
