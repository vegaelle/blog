Title: Un hackerspace nantais
Permalink: un-hackerspace-nantais
Date: 2013-01-07
Modified: 2013-01-07
Tags: hackerspace, fablab, nantes, bretagne
Category: Hacktivisme

Suite à [un rapide gazouillage](https://status.ndn.cx/notice/1572310), une
réunion s’est aujourd’hui improvisée dans un bar de Nantes, dans l’optique de
rassembler des gens intéressés pour la création d’un hackerspace. Compte-rendu.

<!-- cut -->

### Qu’est-ce qu’un hackerspace ?

<p><img src="/images/hackerspace-nantes/mateist.png" alt="Mateist"
title="Mateist — CC-BY-SA HSBP" width="299" height="234" class="aligncenter
size-full" /></p>

Je ne pense pas qu’il y ait de définition académique. Alors voici ma définition
très personnelle. Nous n’avons pas évoqué ce que nous pensions être un
hackerspace, et il me semble pertinent de le faire à l’avenir.

Un hackerspace est avant tout un lieu d’échange. De fait centré sur la
technologie, mais ce n’est pas une limite. En réalité, c’est un endroit
autonome, aussi indépendant que possible, dans lequel les gens viennent partager
leurs connaissances, compétences, expériences, et idées. Par nature, il est donc
raisonnablement anarchiste. À mon sens, il y a 3 notions à respecter
impérativement :

- **ouverture**. Parce que malgré l’image élitiste que les gens peuvent parfois
avoir, il est important que le hackerspace soit ouvert à tous, et à tout. C’est
à dire, en premier lieu, aucune discrimination. On accueille toute personne
souhaitant y entrer, avant tout parce qu’on n’a pas à avoir de l’autorité sur
elle. Ouvert à tout, également. Parce que quiconque veut faire quelque
chose peut le faire. Ce n’est pas pour autant que tout le monde doit y
participer, mais il ne faut pas, par exemple, décréter qu’un hackerspace ne fera
pas de domotique, sous prétexte que ça ne semble pas suffisamment important pour
mériter qu’on y consacre du temps. Ajouter des règles restrictives serait
totalement contre-productif et à l’opposé des valeurs défendues.
- **partage**. Naturellement, c’est un lieu de mise en commun de connaissances.
S’il n’est pas interdit d’entrer dans un hackerspace pour se poser sur un coin
de table et bosser tout seul, il faut admettre que ça n’a pas vraiment 
d’intérêt et qu’il vaut mieux le faire chez soi. La véritable valeur ajoutée
du hackerspace, c’est la possibilité de travailler ensemble, d’apprendre des
autres, et d’apprendre aux autres ce que l’on sait faire, en retour (ou pas
d’ailleurs, il ne s’agit pas d’« un prêté pour un rendu »), de mutualiser de
l’outillage, et de partager ensuite au reste du monde ce que l’on conçoit, au
travers des [licences libres](http://fr.wikipedia.org/wiki/Licence_libre). 
- **fun**. C’est peut-être surprenant de le trouver parmi les valeurs clés d’un
tel lieu, mais c’est pour moi crucial. Les hackerspaces sont gérés par leurs
membres, qui ne sont pas des employés. Personne n’est payé pour venir, alors il
faut garder à l’esprit que, si les gens viennent, c’est par envie. Il est donc
important de renouveler l’envie en prenant du plaisir à faire ce qu’on y fait,
en toutes circonstances. Que ce soit en travaillant sur des projets ou en
prenant soin du lieu, il est inefficace de le faire sous la contrainte. Ne
forcez personne à travailler avec vous sur un projet : s’il n’intéresse
personne, faites-le ailleurs (ou tout seul au hackerspace si vous voulez), mais
ça ne sera pas un mal (ou une défaite personnelle). Juste pas la bonne
opportunité.

C’est une définition très théorique, en fait, parce que je ne vois pas comment
expliquer plus concrètement. Je pourrais expliquer ce qu’est le
[Nicelab](//blog.nicelab.org), par exemple, mais chaque hackerspace est unique.
En gros, on peut s’attendre à un bout de garage avec des barbu·e·s passionné·e·s
qui bricolent des trucs étranges, codent pendant des heures en buvant de la
bière, soudent ou dessoudent des appareils. Ou ça peut être complètement
différent.

### État des lieux

Je débarque fraîchement à Nantes, après avoir passé l’essentiel de ma vie à
Nice. J’ai participé à la fondation du Nicelab, lancé en septembre 2011, et j’ai
été depuis l’un des membres les plus actifs, ce qui me donne une certaine
expérience pour faire la même chose en mieux ici. Car, bien qu’il y ait
énormément de dynamisme, il n’y a pas encore de vrai hackerspace. En partie
parce que les gens se sont focalisés sur la création du [FAI
local](http://ffdn.org) [Faimaison](http://faimaison.net), ce qui est une bonne
chose. Maintenant que Faimaison tourne, et profitant de mon arrivée, j’ai
commencé à secouer les choses pour relancer l’idée.

C’est un projet qui peut donc intéresser les gens de Faimaison, dont certains
suivent l’idée de très près. Également les libristes de [Linux
Nantes](http://linux-nantes.org), ou encore des hackers non-affiliés, qui
seraient ravis de pouvoir partager leurs activités. Nantes est très dynamique à
ce niveau, et entre Numerama et la Cantine Numérique d’un côté, et la myriade de
start-ups technophiles de l’autre, un hackerspace sera une nouvelle pièce de
brassage culturel.

Enfin, il y a déjà un fablab à Nantes, et d’après ce que j’ai compris, il est
sur le point de faire une bouture. Les fablabs sont sensiblement différents des
hackerspaces, entre autres pour leur finalité : même s’il est souvent possible
dans un hackerspace de construire des choses, la finalité reste le partage de
connaissances, et non la construction en soi. Peu importe que les gens viennent
pour *faire* ou pas, dans un hackerspace. En bref, c’est assez complémentaire du
hackerspace. Il y a d’ailleurs peut-être une opportunité là-dedans : le fablab
pourrait fournir le local du hackerspace. Mais c’est pour l’instant une idée en
l’air, et il faudra que tous les intéressés se rencontrent.

### What do?

D’abord, en discuter. On l’a déjà fait ce soir, on était 4. Ce qu’on a
rapidement dit, c’est qu’il semblait pertinent de s’installer en premier lieu à
B17, le local associatif utilisé notamment par Faimaison et Linux Nantes. C’est
un lieu sympa, grand, en centre-ville, très intéressant pour se retrouver. Il y
a des PCs, du réseau, des tables, il n’y a pas besoin de plus pour lancer une
dynamique. Si on peut y stocker une caisse de matériel et d’outillage pour faire
un peu d’électronique de base, ça couvrira déjà 80% des activités que j’ai pu
voir en un an d’existence du Nicelab. Alors certes, ça implique de mutualiser le
lieu, donc de ne pas l’ouvrir comme bon nous semblerait, et ce sera compliqué
d’y laisser du matériel un peu lourd (comme une perceuse à colonne). Mais il
faut savoir faire des compromis, sinon on n’est pas prêts d’avancer. Et pour ne
pas décider de trop de choses à 4, même si je suis assez partisan d’un
fonctionnement
[faisocratique](//blog.nicelab.org/la-faisocratie-en-pratique.html) pour un tel
projet, je proposerai dans les prochains jours de faire une nouvelle réunion, 
dans le but de faire connaissance et de commencer à échanger sur des projets.
La suite se planifiera à ce moment, je n’ai pas de visibilité sur la façon
dont les choses vont évoluer, étant donné qu’idéalement, il s’agira de la somme
de ce que tous les membres y apporteront.
