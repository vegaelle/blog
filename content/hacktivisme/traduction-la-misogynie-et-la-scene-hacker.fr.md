Title: [Traduction] La misogynie et la scène hacker
Permalink: traduction-la-misogynie-et-la-scene-hacker
Date: 2012-03-09
Modified: 2012-03-09
Tags: datalove, misogynie, okhin, traduction
Category: Hacktivisme

*Oui, encore un billet traduit d’[Okhin](https://about.okhin.fr/posts/Misogyni_and_Hackers/). Parce que ce garçon est franchement intéressant et qu’on est d’accord sur plein de trucs. Ce qui ne m’empêche pas d’écrire pour moi, même si ce blog n’est pas très actif : je commets des billets chez le [Nicelab](https://blog.nicelab.org) et on m’[invite](http://seteici.midiblogs.com/archive/2012/03/31/internet-n-existe-pas.html) parfois à écrire.*

<!-- cut -->

### La misogynie et la scène hacker

Depuis un moment maintenant, je lis et entends un tas de choses au sujet de l’identité sexuelle et en particulier autour du milieu hacker. D’un côté, des gens écrivent des choses comme [celle-ci](https://boumbox.wordpress.com/2012/03/29/les-geeks-vs-le-sexe-feminin/) et de l’autre, j’entends parler de plein de saletés qui arrivent aux gens à propos de leur identité sexuelle (discriminations, harcèlement, fausses accusations de viols, vraies accusations de viols…).

Ça me met mal à l’aise et ça m’énerve parfois parce que j’ai toujours considéré la communauté des hackers (peu importe le sens qu’on lui donne) comme une regroupement d’expériences sociales et comme une tentative de construction d’un monde meilleur. Vous pouvez me traiter d’idéaliste si vous le voulez.

Ho et si vous pensez que du fait de mon identité sexuelle ou parce que je suis un homme, je ne suis pas habilité à en parler, et bien fermez-la et lisez.

Et oui, il s’agit d’une réécriture de ce post, parce que mes idées étaient trop chaotiques pour rédiger un bon billet du premier coup.

### L’utilité de l’identité sexuelle

Les seules utilités que je vois à l’identité sexuelle, c’est-à-dire les situations pour lesquelles l’identité sexuelle est une information nécessaire, sont toutes liées au sexe. Le seul moment où vous avez besoin de savoir si quelqu’un est une fille, est lorsque vous êtes attiré(e) par les filles et que vous voulez vous envoyer en l’air (ou engager une relation amoureuse, peu importe).

Ça veut dire que si vous utilisez une partie de votre message pour m’indiquer cette information, vous vous attendez à ce que je le prenne en compte et dans le cas spécifique de l’identité sexuelle, vous voulez que je vous considère comme sexuellement disponible.

### Au début, il y avait les cyberspaces

#### Dans le cyberspace, personne ne sait que vous êtes un chien

Le cyberspace est un espace d’information pure. Votre [identité](https://about.okhin.fr/posts/Privacy) est la quantité de données que vous émettez et, étant donné qu’Internet était originalement essentiellement basé sur du texte, personne ne pouvait savoir à quoi vous ressembliez. Vous pouviez être un garçon se faisant passer pour une fille qui penserait être une chatte.

C’est toujours vrai dans la plupart des cyberspaces, où vous n’avez pas à choisir une identité sexuelle ou à vous définir selon des informations de *viande*.

Donc, dans les cyberspaces, vous pouvez parfaitement vivre sans connaître l’identité sexuelle de vos interlocuteurs, à part si vous recherchez du sexe, auquel cas vous avez besoin de définir votre identité sexuelle.

#### Hein, « cyberspace**s** » ?

Oui, il y a différents cyberspaces. Il y a ceux qui sont sociaux, où les gens vont juste pour être avec des gens, discuter, organiser des rencontres, ou juste se comporter bizarrement dans un groupe qui partage plein de références à l’humour absurde et insensé que personne ne peut comprendre sans faire partie de ce groupe.

Et il y a les espaces où les gens partagent des informations techniques, cherchent les solutions à leurs problèmes, dans lesquels de travail est effectué. Ce sont les cyberspaces où les hackers font des trucs.

Il y a également les cyberspaces réservés aux bots, ou aux monstres tentaculaires. Il y a beaucoup de ruelles obscures, mais c’est ainsi que sont les cyberspaces.

#### Les filles ne codent pas

Tout comme les garçons. « Garçon » ou « Fille », ou « Queer » sont des identités sexuelles. Les identités sexuelles font du sexe, pas du code. Je suis parfaitement conscient que l’identité sexuelle est une importante partie du « soi », mais ce n’est pas la partie qui écrira du code.

La partie qui écrit du code est la partie « hacker ». Elle n’est pas reliée au genre, à l’identité ou orientation sexuelle de la personne. Quand quelqu’un se connecte et dit « Hé, je suis une fille, je veux apprendre le Python ! », on lui répond « Les filles ne codent pas. » (au mieux).

Alors, s’agit-il d’une politique du « ne demande pas, n’en parle pas » ? Et bien, oui et non. Dans un contexte technique, dans le cas où vous voulez apprendre des choses, votre identité sexuelle est impertinente. Elle n’a aucun intérêt. Si vous l’utilisez pour obtenir de l’aide, ça veut dire que vous pensez qu’être différent(e) favorisera les réponses à cause de cette différence. Vous utilisez votre identité sexuelle pour obtenir ce que vous voulez ? Alors ne vous plaignez pas de n’être vue que comme une paire se seins.

#### Nous sommes définis par ce que nous faisons

Un autre gros sujet parmi les communautés de hackers est la [faisocratie](http://blog.nicelab.org/la-faisocratie-en-pratique.html). Nous interagissons les uns avec les autres selon ce que font ces autres, pas selon leurs apparences.

Notre richesse est basée sur nos connaissances et nos compétences, que nous tâchons de partager au mieux et non sur des choses que l’on peut acheter. La plupart des discriminations physiques auxquelles nous pouvons être confrontés dans la viande n’ont pas lieu d’être dans le cyberspace, du moment que vous ne les utilisez pas pour définir votre identité.

Dans les cyberspaces, nous avons une opportunité unique d’ignorer toutes les discriminations basées sur la nationalité, le genre, l’identité et l’orientation sexuelle, la couleur ou le handicap. Chaque fois que quelqu’un se définit sur l’un de ces critères, cela demande beaucoup de self-contrôle pour tous les autres pour ne pas l’insulter. Pour ceux qui font attention à ne pas insulter les gens, en tout cas.

Nous ne nous connectons pas pour socialiser. La plupart d’entre-nous sommes en ligne parce que c’est un moyen facile de partager des points de vue avec des gens à l’autre bout de la planète. Si nous voulions nous envoyer en l’air, nous serions sur d’autres cybespaces, mais ce n’est pas notre but. Quand une fille arrive et dit « Hé, regardez, je suis une fille ! », on lit généralement « Hé, regardez ! Nichons ! », parce que c’est ainsi qu’elle a voulu être considérée (sinon elle n’aurait pas indiqué qu’elle était une fille).

#### Voilà les trolls

À propos des « plaisanteries » en ligne et des mèmes sexistes qui émergent des cyberspaces : la plupart d’entre eux vient de /b/ et il y a une règle pour ça. La règle numéro 1 d’Internet. “Don’t talk about /b/.”. Et vous n’êtes pas forcés d’y aller.

Ces mèmes ne sont pas le problème. L’humour peut offenser les gens et il le fait, surtout l’humour basé sur l’identité de quelqu’un. Et oui, je peux parfaitement comprendre que certaines blagues ne soient pas drôles pour tout le monde et offensent certaines personnes.

Il existe un tas de sujets qui offensent les gens. Je pourrais, par exemple, être offensé par le fait que vous me voyiez comme un mec qui n’arrive pas à s’envoyer en l’air, ou comme l’intello aux grosses lunettes, ou le type bizarre du groupe, ou le techos qui va arranger toute la torture que votre matériel électronique vous inflige simplement en vivant avec vous. Et vous pourriez être offensée que je vous voie comme une paire de seins juste parce que vous m’avez dit que vous êtes une fille.

Oui, il y a des trolls sexistes, mais aussi des racistes, antisémites, ou BSDistes (les pires de tous, si vous voulez mon avis). C’est la pire partie du cyberspace, celle dont personne n’est fier, mais une part nécessaire. Car dès que cette partie disparaîtra, ça voudra dire que nous aurons atteint une forme sérieuse d’autocensure pour un soi-disant plus grand bien. Vous voulez combattre dans l’arène des trolls, très bien mais soyez prévenus qu’ils mordent.

### Mais nous sommes faits de viande

Et là est tout le problème. Votre corps porte de nombreuses informations. Quand je vous verrai, avant de connaître votre nom, je connaîtrai votre genre, votre taille, votre couleur de peau, votre poids, votre attractivité, etc.

Toutes ces informations, que nous n’utilisons pas dans la majorité de nos interactions (car je passe plus de temps à parler aux gens dans le cyberspace que parmi la *viande*) sont obligatoires, un peu comme sur Facebook. Et vous ne pouvez pas les falsifier, contrairement à Facebook.

L’autre problème de la *viande* est qu’il n’y a aucun bouton « ignorer », « filtrer » ou « quitter ». Vous êtes forcé d’interagir avec les gens et ne pas répondre à quelqu’un est considéré comme impoli. Les règles diffèrent pas mal et parfois nous tendons à l’oublier.

Lorsque nous rencontrons « en vrai » des gens que nous connaissons déjà dans un cyberspace, on peut devenir extrêmement maladroit, mais la plupart du temps on parvient à y faire face. Et, à cause des pseudonymes, il peut arriver qu’on se rencontre sans faire de lien avec un nom (et ça arrive souvent). Alors les problèmes se posent principalement avec les gens qu’on ne connaît pas encore.

Les gens qui fuient ces rencontres, terrorisés parce qu’ils auront été vexés, sont une chose regrettable. Soit ces personnes auront simplement paniqué parce qu’elles n’auront pas compris quelles étaient les problématiques et les règles sociales, soit les initiés ont été méchants et ont oubliés qu’ils ne pouvaient être mis en */ignore*.

#### Est-ce qu’il y a un RFC pour ça ?

Je pense que nous sommes conscients de cette situation. Et ce n’est pas quelque chose qu’il est facile d’arranger, particulièrement lorsque les deux camps ne partagent pas les mêmes règles. Les féministes tendent à se définir comme des femmes *(du moins pour les femmes féministes, NDG)*, tandis que nous nous définissons comme hackers. C’est quelque chose de naturel, étant donné que leur combat est pour l’égalité des personnes, peu importe leur identité sexuelle, tandis que le nôtre est la récolte et le partage de tout type de savoir nécessaire pour comprendre le monde.

Dès lors qu’il y a une différence, il y a une discrimination. Les filles se plaignent que nous *(hé, je me place pas là-dedans, moi ! NDG)* soyons sexistes, elles devraient voir le sort qu’on réserve aux gens sous Windows. Le vrai problème est probablement que la plupart d’entre-nous se foutent bien de ces problématiques. Ce n’est pas un problème, c’est comme ça que ça fonctionne dans une faisocratie. Tout le monde ne cherche pas à s’émanciper des serveurs DNS racines, ou ne travaille pas sur un nouveau protocole de maillage dynamique ; les gens qui s’intéressent à ces problématiques y travaillent et lorsqu’ils atteignent leur objectif, ils en feront une jolie présentation à une conférence, ou publieront quelque part leurs travaux.

Le problème du sexisme est simplement un autre sujet. Il y a pas mal de personnes qui s’y penchent et ça commence à avoir de la visibilité. Nous sommes conscients de cela et du fait que ce n’est pas solvable par un <acronym title="Request For Comments">RFC</acronym>.

#### *Problem, officer?*

Nous sommes un peu rudes sur les bords. Et il nous arrive d’être impolis sans nécessairement nous en rendre compte. C’est plus une question de misanthropie latente que de misogynie latente. Lorsque nous sommes dans un hackerspace, nous ne sommes pas là essentiellement pour socialiser et il n’y a rien de plus ennuyeux que les gens qui se pointent en disant « Hé, salut, je m’appelle Luc ! ».

Il y a un autre truc que je déteste : la discrimination positive. Je ne ferai pas d’efforts pour être particulièrement sympathique avec les filles en particulier. J’essaie d’agir de la même façon avec tout le monde (oui, ça signifie être un connard pour tout le monde).

Je pense qu’il y a également une petite proportion d’entre-nous qui sont socialement inadaptés. Ils ne pigent pas, ou ne veulent pas piger, les conventions sociales. Je persiste à penser qu’ils ne sont pas la majorité d’entre-nous, mais ils sont ceux qui correspondent au cliché que tout le monde se fait sur les hackers. Virez-moi ce cliché et vous verrez qu’il y a plein de gens intéressants qui discuteront avec vous d’un tas de sujets différents.

Mais vous devez admettre que, même dans les conventions sociales considérées comme normales par la plupart des personnes, monopoliser l’attention de quelqu’un avec un sujet inintéressant est impoli. Ça ne me pose aucun problème. Vous n’êtes pas intéressé par la construction d’un quadricoptère, dites-le moi, j’arrêterai de vous embêter avec ça. Si vous venez me voir et commencez à me parler d’un sujet qui ne m’intéresse pas, je vous le dirai et vous n’aurez qu’à faire avec, car ce ne sera pas à cause de vous, mais seulement que le sujet duquel vous souhaitez me parler n’a aucun intérêt pour moi.

### Et ça devient physique

C’est là le problème. Nous sommes loin d’être parfaits. Certains d’entre-nous tendent à se considérer comme des héros qui sauvent le monde. Certains d’entre-nous sont véritablement des sociopathes qui n’en ont rien à faire d’écraser des gens du moment qu’ils ont ce qu’ils veulent. Mais ces gens sont partout, pas seulement là où on se regroupe.

Lorsque ça en vient au physique, lorsque quelqu’un essaie de s’attaquer à une personne, que ce soit par le harcèlement, l’intimidation, ou l’agression sexuelle, nous nous devons d’intervenir. Je pense que la façon chaotique dont le monde des hackers fonctionne nous donne la possibilité d’essayer de résoudre ce problème.

Je ne sais pas comment le faire. Mais je ne pense pas que nous soyons capables de nous faire aux règles sociales avec lesquelles vous jouez. Vous voulez interagir avec les hackers ? Devenez-en un. Ensuite, si vous avez des problèmes, parlez-en publiquement, documentez les différents cas, trouvez un moyen de travailler en contournant ces problèmes, chargez-vous en. Nous ne pouvons apporter de solutions lorsqu’il semble que nous soyons le problème.

Ce billet a l’air de servir d’excuses pour certains. J’essaie juste de comprendre comment les choses fonctionnent. Je suis particulièrement chanceux, j’ai essuyé peu de discriminations ces 10 dernières années (et les quelques-unes que j’ai subies étaient dues au fait que je me comportais bizarrement volontairement) et il se pourrait que je n’ait pas la légitimité pour parler de ça.

### Hors-sujet

Je n’ai pas parlé de porno, ou du fait que peu de filles vont en écoles d’ingénieurs, parce qu’il s’agit d’excuses et de symptômes, pas de causes. Je n’ai pas utilisé l’excuse du « mon enfance a été un enfer alors vengeons-nous sur les autres » non plus, parce qu’elle pourrait justifier n’importe quoi. J’ai essayé d’expliquer comment je percevais le problème de mon point de vue, pour en comprendre les origines.

Je persiste à penser que c’est un problème périphérique (mais néanmoins non mineur), mais se focaliser sur le problème de l’identité sexuelle est, pour moi, une erreur. Nous ne devrions pas discriminer. Point.

*Edit : Je remercie chaleureusement Tris pour l’aide à la correction de cette traduction.*
