Title: Gratuit pour les filles ? Sérieusement ?
Permalink: gratuit-pour-les-filles-serieusement
Date: 2012-06-25
Modified: 2012-06-25
Tags: féminisme, hack, nuit du hack
Category: Hacktivisme

<p><img src="/images/sexisme-ndh/ndh_sexisme.png" alt="Sexisme à la Nuit du Hack" title="Sexisme à la Nuit du Hack" width="800" height="201" class="aligncenter size-full" /></p>

J’ai toujours été vaguement choqué par ce genre d’« offre commerciale », bien avant de prendre conscience de l’importance de la lutte pour l’égalité sociale des genres. Et, après une récente discussion à ce sujet et, surtout, un [revirement d’opinion](https://twitter.com/jujusete/status/216468634405576705), je pense utile de poser mon opinion par écrit.

Les organismes, sociétés, ou autres, qui offrent l’entrée à un évènement ou lieu aux filles font preuve d’un sexisme sans appel. Voilà pourquoi.

<!-- cut -->

### « On ramène de la viande fraîche »

C’est bien évidemment l’argument souhaité par les organisateurs. Ramener des filles. Mais, quand on y pense, dans quel but ? Lorsque c’est pour une boîte de nuit, c’est naturellement pour favoriser les rencontres, et à la rigueur, cette volonté peut être louable, plus que la façon d’y parvenir du moins. Par contre, quand c’est dans le cadre de la [Nuit du Hack](https://www.nuitduhack.com/), un évènement très ciblé, sans le moindre lien avec le [meatspace]({filename}/hacktivisme/traduction-la-misogynie-et-la-scene-hacker.fr.md), où se rencontrent des compétences de toutes sortes, ça me dérange profondément. Pour quelle raison voudrait-on faire en sorte qu’il y ait plus de femmes ? Favoriser les rencontres ? Mais c’est totalement hors de propos ! Les gens qui se rendent à cet évènement y vont pour apprendre, pour s’entraîner, certainement pas pour y rencontrer l’âme sœur ou un quelconque plan cul ! Et surtout, considérer uniquement un sexe de la sorte est un message lancé à tous les visiteurs : « les filles qui seront là sont présentes avant tout pour régaler vos pupilles ». On insulte proprement toute compétence intellectuelle des femmes, en les rabaissant à un simple bout de viande tout juste bon à être regardé par les hackers mâles, qui eux ont des compétences dignes d’être prises au sérieux (quand bien même beaucoup ne viennent que par curiosité, sans être des brutes de pentesting). J’espère ne surprendre aucun de mes lecteurs (d’autant plus après le billet d’Okhin) en rappelant qu’avoir des compétences est parfaitement distinct de son genre, qu’il soit naturel ou décidé. Évidemment, ce genre d’évènement a lieu dans le meatspace, mais je pense qu’il est important d’y importer cet aspect du cyberspace : la non-personnification des « gens ». Quelqu’un qui hack n’est ni un homme ni une femme, mais un individu.

### Permettre aux hackers (nécessairement mâles) de se faire accompagner par leurs copines (nécessairement potiches)

Pour la NdH, je pense que c’est l’argument qui a conduit à ce choix. Mais c’est involontairement très vexant. Pour les raisons que je viens d’écrire. Ça stéréotype à l’extrême les gens de ce milieu. Il est vrai que les hackers sont majoritairement des hommes, c’est une statistique et l’on n’y peut rien. Par contre, favoriser cet état de fait en considérant que ceux qui viendront à cet évènement seront forcément des hommes est malsain. Les femmes hackers se rendant à la NdH (que ce soit seules ou accompagnées de quelqu’un qui n’y comprendra rien) sont insultées : on leur dit « c’est payant, mais parce que tu es une femme, tu ne comprends tellement rien au sujet qu’il ne serait pas juste de te faire payer ». Encore une fois, c’est involontaire, bien sûr. Mais la plus dure tâche du combat féministe est bien de lutter contre ces discriminations du quotidien. Et quid des couples différents ? Savoir que le milieu hacker, censément plus ouvert que la moyenne, s’abaisse à cette considération me chagrine profondément. À en croire certain, le hacker est un mâle hétérosexuel, point. C’est une vision des choses qu’il est impératif de hacker. Je n’ai malheureusement pas la possibilité d’y être, mais je participe à ma manière, en écrivant ce billet. J’espère que des gens sur place sauront se faire entendre.

### Et si on appliquait nos principes ?

Sérieusement, ça serait vraiment trop difficile d’éviter ce genre d’actions débiles et discriminantes ? De faire payer les visiteurs, quels qu’ils soient ? D’abord, ça permettrait de rapporter un peu plus de financement pour l’évènement, ce qui est une bonne chose. Mais aujourd’hui, est-ce que des femmes viennent seulement parce que c’est gratuit ?

[Ju](http://seteici.ondule.fr/) a changé d’avis là-dessus en voyant des couples heureux se balader. Est-ce que ça n’aurait vraiment pas pu être le cas s’ils avaient du payer deux billets ? Et puis, si les filles devaient payer comme tout le monde, pourquoi ne pas baisser le prix des billets, après tout ? Autre possibilité, pourquoi ne pas opter pour un [financement collaboratif](http://fr.ulule.com/thsf-2012/) de l’évènement, comme l’a fait le [THSF](http://thsf.tetalab.org/) ? C’est une solution qui a fait ses preuves, qui fonctionne souvent mieux que prévu, qui permet à des gens le donner plus qu’on ne leur demande, simplement parce qu’ils savent qu’ils permettent l’existence de l’évènement, et à l’inverse, si certains veulent participer mais n’ont pas forcément les moyens de se payer une place, ils en ont la possibilité.

### C’est dans les deux sens

J’ai enfin été choqué par des réponses au tweet de Ju précité : « oui, c’est sexiste, mais ça me paie le restau pour ce soir ». C’est peut-être même pire que l’acte en lui-même. L’acceptation de la discrimination si elle va dans notre sens est une forme de légitimisation du sexisme en général. Il ne faut pas alors s’étonner d’entendre des hommes dire qu’ils trouvent que les femmes *exagèrent avec les inégalités salariales, avec le viol, parce que c’est pas si grave que ça voyons…* Non, ce n’est pas plus normal, dans un sens ou dans l’autre. Et quand on choisit de se battre pour l’égalité, ça ne signifie pas « être plus égaux que les autres ». Beaucoup se moquent du féminisme, en prétendant qu’il s’agit de « femmes qui veulent dominer le monde ». Il faut bien constater que c’est trop dur pour beaucoup d’oser se remettre en question et d’accepter que la *simple* égalité sociale est méritée.

**Edit :*** Ju a écrit sur [le même sujet](https://seteici.ondule.fr/2012/06/gratuit-pour-les-filles/), mais pour défendre le point de vue opposé. Je vous encourage à le lire.

<!-- cut -->
**Edit 2 :** Étant donné la migration de ce blog sous un format statique, il n’est plus possible de commenter les articles. Cependant, celui-ci avait généré une intéressante discussion, recopiée ici telle quelle.

> **Le 23 juin 2012 à 14 h 50 min, [Noname](http://duckduckgo.com/) a dit :**
> 
> Ba c’est surtout pour aider à la reproduction du hacker qui sans ça risquerait une disparition certaine bientôt si on fait rien pour préserver l’espèce mais malheureusement les filles ne jouent pas le jeu d’ailleurs on signale qu’a la nuit du hack il y a beaucoup de garçons déguisés pour ne pas payer le prix de l’entrée .
> > **Le 23 juin 2012 à 14 h 57 min, [Gordon](https://www.gordon.re/) a dit :**
> > 
> > Et ça se voit pas trop, la barbe ? :]
> > > **Le 23 juin 2012 à 15 h 16 min, [Noname](http://duckduckgo.com/) a dit :**
> > > 
> > > ba les portiers font plus attention au décolleté qu’au reste donc ça passe !
> > > Sinon on attend toujours le début de l’atelier t-shirts mouillés!

<span></span>
> **Le 23 juin 2012 à 15 h 03 min, [deadalnix](http://www.deadalnix.me/) a dit :**
> 
> Gordon, tu m’édite ce commentaire pour virer ça et tu me lourde le précédent ? J’ai foiré l’URL de mon site :D
> 
> Je comptais écrire dessus, mais ce que tu racontes la est exactement ce que j’aurais voulu écrire. Donc tout d’abord merci.
> 
> La France est un pays étrange sur le sujet. Il est schizophrène, ne sait pas s’il veut l’égalité des sexes ou non.
> 
> Comment justifier que l’écart des salaires est problématique quand l’on trouve normal que l’homme invite la femme au resto ou ailleurs, par exemple ? Comment veut-on que les femmes soient considérées en politique si elle ne sont la que pour remplir un quota légal, et non pour leur compétences ?
> 
> Dans les pays scandinaves par exemple, la parité est un sujet important. Il est par exemple très mal vu d’inviter une femme lors d’une sortie. Cela signifierait que l’on considère qu’elle est incapable de subvenir à ses besoin besoins et qu’elle a donc forcement besoin d’un homme pour le faire à sa place. En bref, on l’insulte. Dans l’autre sens, on ne constate pas les mêmes écarts de salaires que ce que l’on peut voir en France.
> 
> C’est un sujet sérieux sur lequel la France doit faire un choix. Ou bien on veut l’égalité, ou bien on ne la veut pas, mais la situation actuelle n’a strictement aucun sens.
> 
> > **Le 24 juin 2012 à 17 h 28 min, [Ju](http://seteici.ondule.fr/) a dit :**
> > 
> > Ce que j’aime chez Gordon et toi, ce pourquoi vous êtes aussi mes amis, et ce que je retrouve souvent chez des copains bidouilleurs, c’est cet esprit curieux et cet envie d’égalité puisque dans le cyberspace, nous sommes finalement tous au même niveau (et c’est une très bonne chose.
> > Et tu nous le montres encore plus avec ce commentaire.
> > Ce n’est pas que l’homme qui invite la femme au resto, de mon point de vue, c’est aussi la nana qui l’accepte, le conçoit… alors qu’il est si sympa de s’inviter leur l’autre de temps en temps. Idem pour les cadeaux, etc.
> > Mais il ne faut pas oublier que la Frane est un pays latin, méditerranéen et que, de par son histoire, reste cette image de « domination » masculine, même sur les offres d’emploi. On cherche un/une journaliste mis un rédacteur en chef…

<span></span>
> **Le 23 juin 2012 à 18 h 16 min, deuzeffe a dit :**
> 
> Hello Gordon et lizot,
> 
> Il y a de l’idée, mais il faut pousser plus loin, voire retourner
> l’argument et l’interpréter différemment.
> 
> Comme toi, je mets de côté la gratuité des soirées dansantes, bien qu’on
> pourrait y développer le même genre de retournement (sans jeu de mots,
> merci).
> 
> Vous voulez, nous voulons hacker la société ? Alors oui, commençons par
> nous-mêmes. Commençons par nous libérer de ce leitmotiv bien-pensant, de ce
> confort de pensée, de cette pensée unique : la société n’est pas
> égalitaire entre les genres alors on va tout faire pour la forcer à l’être
> en faisant des lois, des règlements, des quotas, de l’égalitarisme
> imposé, que sais-je encore !?
> 
> Si on pensait autrement ? Si on pensait que « Gratuit pour les filles »
> n’est pas un appât, une condescendance, mais un cadeau, une invite à
> reconnaître que oui, nous ne sommes pas assez nombreuses en info., que
> oui, nos talents de hackeuses (en info. ou dans d’autres domaines) ne sont
> pas assez reconnus parce que, finalement, c’est notre lot quotidien (entre
> autre, celui que nous impose la société à la maison ou au boulot, mais pas
> que). Donc, renversons la charge de la preuve, nous risquons d’y voir plus
> clair et il se peut que cela devienne efficace.
> 
> Pourquoi les filles excellent au lycée puis partent sur des filières
> supérieures courtes comme si elles n’osaient pas pousser leurs études plus
> loin (je te laisse retrouver les réf. INSEE qui vont bien) ? Pourquoi les
> trouve-t-on plus en bio. qu’en sciences dures ? [là il y a un développement
> à faire qui n'a pas trop sa place ici] Pour quoi ? Pour qu’un pré carré
> soi-disant masculin soit préservé ? Je n’y crois pas non plus : les études
> socio. (pareil, je te laisse trouver les réf.) montrent que les dirigeantes
> ont à coeur de faire fructifier leur territoire et sont beaucoup efficaces
> dans la gestion des entreprises que les dirigeants qui sont plus enclins à
> l’étendre au risque de faire sombrer la structure par manque de solidité
> de fond. Donc pour quoi ?
> 
> Le « Gratuit pour les filles », c’est un cadeau d’égalité de respect qui
> nous est fait : la société ne vous permet pas, à vous les filles de
> participer de tout ce que vous êtes intellectuellement et moralement ? Qu’à
> cela ne tienne ! Venez, entrez, on /sait/ que vous avez des talents qui
> s’expriment différemment des nôtres, on /sait/ que vous pouvez vous sentir
> frustrées de ne pas pouvoir participer à votre juste valeur, alors, entrez,
> /nous/ on vous fait votre place.
> 
> Hackons notre pensées, nos mots, le hack matériel viendra de lui-même.
> 
> Mes 2F^Hcents.
> > **Le 23 juin 2012 à 18 h 46 min, [Gordon](https://www.gordon.re/) a dit :**
> > 
> > Je ne suis pas du tout d’accord avec toi.
> > 
> > Tu parles de lois, quotas, égalité forcée. Ce n’est évidemment pas une solution. Il faut éduquer les gens pour faire considérer l’égalité des sexes comme normale, pas l’imposer par la rigueur. Ça n’a jamais marché, et ça ne marchera pas plus.
> > 
> > Ton principal argument est que la gratuité pour les femmes serait un « cadeau »… Dans ce cas, pourquoi le rôle des femmes ne serait-il pas de s’occuper des tâches ménagères, en cadeau pour les mâles qui mettent à manger sur la table ? Perso, je ne vois aucune différence. Et c’est ce que j’explique dans mon dernier point : ça me chagrine, parce qu’à partir du moment où on tire un profit personnel d’une inégalité, celle-ci ne semble plus importante.
> > 
> > En l’occurrence, que représente le prix d’entrée de la NdH ? La participation aux frais. Parce qu’on considère que les présents profitent de l’organisation, et doivent donc contribuer à ce qui a été mis en place pour eux. Comment peut-on alors justifier la gratuité pour une catégorie de personnes ?
> > 
> > Par ailleurs, tu parles de preuve de respect pour les femmes « techos », mais il est tout aussi faux de penser que toutes les femmes venant à un tel évènement sont des brutes, que de penser la même chose des hommes. Certain(e)s viennent par curiosité, d’autres pour accompagner, d’autre encore pour gagner le CtF… Qu’ils soient hommes ou femmes. Pourquoi devrait-on alors rendre hommage aux compétences de toutes les femmes présentes, et pas aux hommes, alors que dans les deux cas, il y a des gens très compétents comme des touristes ?
> > 
> > En gros, ce que tu défends ici, c’est justement la discrimination positive. Je n’aime pas du tout cette idée, parce qu’à un moment où un autre, elle retombera dessus, et aura l’effet parfaitement inverse. Par ailleurs, elle favorise encore une fois la différenciation des deux catégories, alors qu’il n’y aurait pas la moindre raison de le faire ici.
> > 
> > Je reste persuadé qu’on ne lutte pas contre une discrimination par la discrimination, mais par l’éducation et l’acceptation.
> > 
> > PS : « a société ne vous permet pas, à vous les filles de participer de tout ce que vous êtes intellectuellement et moralement ? »
> > Bien sûr que si, elle le permet.
> > > **Le 23 juin 2012 à 19 h 34 min, deuzeffe a dit :**
> > > 
> > > C’est bien ce que je craignais : je n’ai absolument pas réussi à faire comprendre que ce n’est pas de la discrimination positive. Tant pis.
> > > 
> > > PS : regarde la proportion des femmes dans différentes professions et tu verras que la société les cantonne systématiquement toujours dans les mêmes domaines, même s’ils ne correspondent pas à leurs capacités intellectuelles ou morales. Combien de laissées pour compte pour une NKM, une F. Pellerin, une L. de la Raudière ou une M. Billard ?
> > > 
> > > Et la société, c’est nous tous…
> > > > **Le 23 juin 2012 à 19 h 42 min, [Gordon](https://www.gordon.re/) a dit :**
> > > > 
> > > > Je suis parfaitement d’accord avec ça, mais j’avoue que je ne comprends pas le rapport. Je sais bien que lorsque les quotas de parité sont imposés, on utilise les femmes pour les postes les moins importants, et c’est ce que je voulais dire par « l’égalité forcée est une mauvaise solution ».
> > > > 
> > > > Par contre, je ne vois pas en quoi faire ce geste peut être un cadeau. À première vue si, bien sûr, parce que fatalement, elles économisent de l’argent, mais je pense qu’on devrait considérer tous les visiteurs de la même façon. La meilleure façon de montrer l’égalité, c’est précisément ne pas montrer de différence de comportement.
> > > > > **Le 23 juin 2012 à 20 h 12 min, 2A a dit :**
> > > > > 
> > > > > Hello, je suis assez d’accord avec miss deuzeffe en fait.
> > > > > Gordon, je suis d’accord avec toi aussi :o )
> > > > > 
> > > > > Mais amha on ne PEUT pas actuellement faire comme si notre societe etait juste, egalitaire ou que sais-je … Avant de causer d’egalite, il faut se dire qu’il y a un lourd retard a rattraper. Est ce qu’actuellement les filles peuvent tout faire comme les hommes ? Non.
> > > > > Donc faire des « cadeaux » (qui seraient plutot dus a mon sens) ? Oui :)
> > > > > 
> > > > > D’ailleurs pourquoi n’offrir que l’entree en boite, ou une conso pour les nanas ?? (1/ramener de la viande, 2/ la mettre a disposition du public masculin ?)
> > > > > N’est il pas la le soucis ?
> > > > > 
> > > > > Donc dans l’immediat le point de vue de deuzeffe me semble etre plus qu’urgent, et quand les choses auront changees on pourra faire payer les filles !
> > > > > 
> > > > > > **Le 23 juin 2012 à 20 h 31 min, [Gordon](https://www.gordon.re/) a dit :**
> > > > > > 
> > > > > > Et si une femme souhaite « faire comme si l’égalité existait » et veut qu’on lui demande de payer sa place comme tout le monde ? Dans ce cas, c’est l’organisation de la NdH qui fait la discrimination…
> > > > > > 
> > > > > > On se heurte à une divergence d’opinion que les arguments pourraient difficilement faire pencher. Soit.
> > > > > > 
> > > > > > Par contre, tu suggères qu’« offrir » la place à la NdH serait en quelque sorte une façon de « s’excuser » pour toutes les inégalités dans l’autre sens ? Je doute que ça soit l’objectif du combat féministe… ni même que ça suffise à accepter le reste.
> 
> <span></span>
> > **Le 24 juin 2012 à 16 h 42 min, [deadalnix](http://www.deadalnix.me/) a dit :**
> > 
> > Je vais être rude, mais des fois il faut.
> > 
> > Tu es une fille c’est bien. Cette info est importante pour moi si je cherche à te sauter.
> > 
> > Quand on parle hacking, montre moi le code. Le reste, c’est du bruit.

<span></span>
> **Le 23 juin 2012 à 19 h 55 min, Kima a dit :**
> 
> Il y a le fait que le milieu de hack est très masculin, et donc l’argument « On est aussi ouverts aux filles, on n’est pas machos ». Mais le résultat est effectivement assez foireux, et ne se distingue pas des offres du même genre, et cette fois ouvertement machistes, que l’on peut voir habituellement.

<span></span>
> **Le 23 juin 2012 à 20 h 34 min, deuzeffe a dit :**
> 
> Il faut commencer tout doucement, avec le moins de rigorisme imposé (pas de quotas, pas de discrimination* positive) : donc, générosité envers les plus défavorisés, love, humanisme, toussa (je force un peu le trait pour essayer de me faire comprendre).
> 
> Et l’égalité, si, c’est avoir un comportement différent, adapté à chaque situation/groupe/personne/etc. pour qu’/in fine/ chacun puisse avoir la même possibilité d’évoluer (en gros). Tu ne donnes pas de chips (au vinaigre) à un bébé pour le nourrir ni de lait artificiel à un athlète de haut niveau pour qu’il explose ses records. (image exagérée mais toujours pour la même raison)
> 
> Et mon propos était aussi (surtout ?) sur le hack de la pensée, du langage.
> * tiens, encore un mot à hacker, ça…
> > **Le 23 juin 2012 à 20 h 45 min, [Gordon](https://www.gordon.re/) a dit :**
> > 
> > « Tu ne donnes pas de chips (au vinaigre) à un bébé »
> > Si !
> > Bon, ok, je ne suis pas prêt à être parent :D
> > 
> > Sinon, les raisons que tu évoques ont une justification médicale. Faire ou pas payer une certaine catégorie de personnes, c’est seulement du social.
> > 
> > Quand à la générosité, je l’exerce différemment, et, je l’espère, de manière plus concrète : je porte plus attention aux discriminations, et je prends bien plus facilement la défense d’une victime de discrimination plutôt qu’une autre (la victime en question pouvant naturellement être un homme, qu’on traiterait par exemple de goujat parce qu’il ne paie pas le restaurant à sa compagne).
> > > **Le 24 juin 2012 à 18 h 56 min, deuzeffe a dit :**
> > > 
> > > Je comprends ton comportement. Mais j’en vois tellement qui, au prétexte de lutter contre La Discrimination, gomment et ignorent les différences, que je crains que cela tourne à l’égalitarisme et non se dirige vers légalité. J’espère me tromper.

<span></span>
> **Le 24 juin 2012 à 16 h 38 min, binnie a dit :**
> 
> hello, je suis l’auteur du tweet « oui, c’est sexiste, mais ça me paie le restau pour ce soir » que tu cites.
> 
> crois bien que c’était évidemment une vanne désabusée. pas une quelconque validation de tout ça.
> 
> je suis complètement d’accord avec ton article, je n’ai juste plus l’envie ou l’énergie de m’insurger contre ce genre de truc, je suis fatiguée de tout ça.
> 
> j’ai fait des études d’informatique ou j’étais la seule fille de ma classe pendant une année entière, je bosse pratiquement qu’avec des mecs depuis dix ans, et je peux te dire que niveau combat féministe, j’ai donné.
> 
> aujourd’hui, je me contente de prendre ma place sans la mendier, en essaynt d’ignorer ce que je trouve anormal. il m’arrive encore de râler, mais je suis tombe dans l’àquoibonisme, notamment quand je me retrouve face à ce genre de trucs de tarification complètement wtf comme pour la nuit du hack. cela dit quand d’autres prennent le relai, qui plus est quand ce sont des garçons, je suis ravie. il ne faut pas laisser passer ce genre de trucs.
> 
> enfin, même si je ne suis pas d’accord avec les organisateurs, je ne voulais pas me passer de ces conférences sous pretexte que l’entrée était gratuite pour les femmes (et tout le truc que ça implique, que tu as très bien résumé.)
> 
> pour moi le plus important reste de me cultiver et satisfaire ma curiosité, avant tout.
> > **Le 24 juin 2012 à 18 h 50 min, deuzeffe a dit :**
> > 
> > Est-ce que tu penses qu’exiger de payer aurait pu changer la vision de choses de certains ?
> > > **Le 24 juin 2012 à 19 h 21 min, binnie a dit :**
> > > 
> > > hello.
> > > 
> > > honnetement, non. je ne pense pas que cela aurait changé quoi la vision de qui que ce soit.
> > > 
> > > il y a encore une distinction des genre trop présente dans ce domaine pour qu’elle soit effacée avec quelques events qui s’employeraient à la gommer. le chemin sera bien plus long que ça.
> > > 
> > > à vrai dire, c’est surtout moi, et beaucoup d’autres garçons et filles, comme je le constate, qui poserions un regard différent, plus bienveillant, sur les choix d’organisation de cet event.
> 
> <span></span>
> > **Le 24 juin 2012 à 18 h 51 min, [Ju](http://seteici.ondule.fr/) a dit :**
> > 
> > héhé ! j’ai meme trollé en te proposant de trouver des chaises en disant qu’on est enceinte (d’ailleurs à ce propos http://seteici.midiblogs.com/archive/2011/04/28/martine-a-la-banque.html ) et oui, il m’arrive de profiter, tant pis pour ceux qui ne cherchent pas les bonnes combines !
> > pour le reste, je suis 100% d’accord avec toi sur la curiosité
> 
> <span></span>
> > **Le 24 juin 2012 à 19 h 06 min, [Gordon](https://www.gordon.re/) a dit :**
> > 
> > Je prends le relais pour la lutte féministe, à mon échelle. Et peut-être que ça te remotivera de savoir que tu n’es pas seule à lutter :)
> > > **Le 24 juin 2012 à 19 h 23 min, binnie a dit :**
> > > 
> > > merci o/
> > > tu vas te faire des copines.
> > > > **Le 24 juin 2012 à 19 h 27 min, [Gordon]() a dit :**
> > > > 
> > > > Pourquoi seulement des copines ? :)
> > > > > **Le 24 juin 2012 à 19 h 43 min, binnie a dit :**
> > > > > 
> > > > > des copains et des copines. #fixed
> > > > > 
> > > > > et je constate que ce tweet https://twitter.com/binnie/status/216918429192556545 a été favé / rt autant par des garçons que par des filles, parmis mes followers.
> > > > > 
> > > > > on y arrive :]

<span></span>
> **Le 24 juin 2012 à 16 h 41 min, binnie a dit :**
> 
> also : y a possbilité de récupérer le programme que tu as développé pour faire des slides text mode comme pour ta prez le week-end dernier à la cantine ? merci :)
> > **Le 24 juin 2012 à 18 h 46 min, toto a dit :**
> > 
> > http://gordon.re/files/t++.git et c’est un fork de tpp (http://www.synflood.at/tpp.html)
> > > **Le 24 juin 2012 à 19 h 01 min, [Gordon](https://www.gordon.re/) a dit :**
> > > 
> > > La banane a très bien répondu. Je ferai prochainement un billet, quand j’aurai implémenté suffisamment de fonctions de base. Bien sûr, vous pouvez d’ores et déjà m’envoyer des patches. Actuellement, la rétrocompatibilité avec TPP est cassée. Je le corrigerai rapidement.
> > > > **Le 24 juin 2012 à 19 h 53 min, binnie a dit :**
> > > > 
> > > > merci.

<span></span>
> **Le 24 juin 2012 à 17 h 08 min, Xavier a dit :**
> 
> Merci pour cet article.
> 
> J’ai été moi aussi choqué par cela.
> 
> À la base, j’accompagnais une amie. Elle est beaucoup plus hackeuse/codeuse que je ne le suis, mais certaines conférences me semblaient intéressantes, donc j’ai acheté mon ticket.
> 
> Arrivés à l’entrée, j’ai présenté mon QR-code à la demoiselle du staff. Code validé, elle me dit « vous êtes un garçon donc vous avez un badge », et me tend également le t-shirt (immettable car taille XL, mais passons).
> Je reste devant le stand d’accueil, j’attends un truc qui n’arrive pas, puis je lance « mais, euh, y’a rien pour les filles ? » Sourire gêné de la staffeuse, « Eh bien non ». Je relance « non mais même pas un badge ? » Négatif.
> Mon amie me pousse vers la salle, lançant un « arrête d’essayer de gratter des trucs » en souriant. J’évoque le sexisme de la chose mais on passe rapidement aux conférence.
> 
> À l’intérieur, de nombreuses filles arboraient fièrement leur badge par-dessus leur t-shirt, donc je pense que la staffeuse de l’accueil a cru que mon amie m’accompagnais gentiment, quand c’était plus l’inverse (elle devait avoir un look trop normal, hohoho). Que même cet état d’esprit se retrouve au sein de l’équipe organisatrice de la NdH me fait un peu mal.
> > **Le 24 juin 2012 à 18 h 30 min, [emilie_1987](https://twitter.com/#!/emilie_1987) a dit :**
> > 
> > Avec le mec de l’accueil j’ai eu le droit au t-shirt ^^

**Ping : [http://seteici.ondule.fr/2012/06/gratuit-pour-les-filles/](http://seteici.ondule.fr/2012/06/gratuit-pour-les-filles/)**

> **Le 24 juin 2012 à 18 h 19 min, [emilie_1987](https://twitter.com/#!/emilie_1987) a dit :**
> 
> Bonjour, je me permets de venir interagir sur le sujet. Pour un peu défendre mon côté féminin dans ce monde rempli d’hommes sans pour autant aller jusqu’à la partie politique.
> 
> Tout d’abord, je ne connaissais pas du tout que ce types de convention existait. En en apprenant plus sur l’histoire de cette nuit du hack dès la première conférence, j’ai tout de suite vu l’ampleur que ça avait.
> Je ne suis pas du tout de ce domaine-là. J’ai un niveau basique en informatique. Mais je suis toujours partante pour connaitre de nouvelles choses et enrichir mes connaissances dans l’informatique.
> Je suis venue accompagner mon partenaire qui lui est ingénieur dans la sécurité en informatique. Et il adore toutes ces choses là. Depuis quelques temps j’essaie un minimum de m’intéresser à ce qu’il fait mais comme dit plus haut, je suis aussi une geekette. Alors j’ai trouvé que justement s’était le moment de montrer que je suis capable de m’intéresser à son monde. En plus, le côtté gratuit était parfait et je connaissais bien les lieux pour y avoir travaillé (j’ai même retrouvé un ancien collègue pour dire).
> 
> Certes, c’est un domaine macho. J’avais l’impression d’être dans une fosse aux lions. Que dès que mon partenaire me laissait seul, y en a un qui serait venu me bouffer. Je me sentais épiée dès qu’il n’était plus là ou qu’il avait le dos tournée.
> C’était le côté dérangeant, et pourtant, je faisais partie des nanas qui étaient habillées classiques et pas en pouf comme j’ai pu le voir en fin de soirée.
> Quand je lisais certains tweets aussi, ça me faisait peur d’être cataloguée de viande fraiche. Hors, j’ai tout de même 25 ans alors je ne suis plus toute fraiche ^^ !
> 
> J’ai pu également revoir une amie qui elle est aussi dans une école d’ingé en informatique et qui m’a initié à ce qu’elle faisait. J’en étais très surprise de voir tout ce qu’elle connaissait. Elle animait un des stands.
> Pour être honnête, pour moi le mot hacker s’arrêtait à la profession hacker les forums des gens et les emmerder à poster des messages multiples ce qui m’a longtemps emmerdé pendant des années lorsque j’étais sur des forums !
> Cette nuit du hack m’a permis de voir ce côté hacker autrement. Et j’ai pu ainsi donc voir comment la réel diversité de ce domaine.
> Et j’étais encore plus surprise de voir que je comprenais toutes les conférences, même en anglais. Quand je ne comprenais pas quelque chose, je n’hésitais pas à demander à mon partenaire. Et le voir aussi sérieux s’était juste trop chou.
> J’ai apprécié surtout les conférences du matin et celle sur le Hacking Mobile. J’en ai même pris des notes dans mon cahier.
> 
> Honnêtement, totu dépendra du lieu de l’année prochaine, mais je serais prête à payer si le prix reste à 50e maximum et je vais même investir dans un ordinateur portable pour la prochaine nuit du hack.
> J’ai pu rencontrer de superbes personnes. Pour moi ce genre d’évènements ne s’arrête pas qu’aux conférences et aux activités du hack. Mais à l’apprentissage, la découverte, la rencontre de nouvelles personnes lié à ce domaine ou non, à l’enrichissement personnel. Et ça ne me gênerait pas du tout d’y aller l’an prochain sans mon partenaire.
> > **Le 24 juin 2012 à 18 h 33 min, [Ju](http://seteici.ondule.fr/) a dit :**
> > 
> > la question est : y serais-tu allée si tu avais du payer 30 euros ? (au passage, contente de t’avoir rencontrée) et je plussoie pour ton amie qui animait l’atelier arduino-led, j’en parle d’ailleurs dans mon billet, super intéressant. Ton propos illustre bien ce que j’écris sur les couples dans mon billet, j’en ai vu plein comme vous.
> > Ah oui, il y avait des pouffes à la fin ? raconte, raconte, raconte !
> > > **Le 24 juin 2012 à 18 h 36 min, [emilie_1987](https://twitter.com/#!/emilie_1987) a dit :**
> > > 
> > > Bah honnêtement je l’aurais fais, je voulais aller au paris-web mais vu le prix, laisse tomber !
> > > Et ça faisait un moment que je voulais aller faire une conf comme celles-là ayant vu les 2 dernières pour PSES ^^
> > > 
> > > Oui, elles se sont montrées aux alentours de 22h30 ^^ elles étaient avec leur copain mais la façon dont elles s’habillaient laissait à désirer.
> 
> <span></span>
> > **Le 24 juin 2012 à 19 h 05 min, [Gordon](https://www.gordon.re/) a dit :**
> > 
> > Juste un conseil, si tu comptes acheter un ordinateur pour l’année prochaine, blinde-le avant de l’apporter ;)
> > 
> > (par exemple, s’il tourne sous windows, je ne lui donne pas 5 minutes de durée de vie à la NdH ;) )
> > **Le 24 juin 2012 à 19 h 10 min, [emilie_1987](https://twitter.com/#!/emilie_1987) a dit :**
> > 
> > T’en fais pas, je demanderais conseille à des pro avant ^^

<span></span>
> **Le 24 juin 2012 à 19 h 29 min, [Biaise](http://biaise.net/) a dit :**
> 
> Débile ce principe de gratuit pour les filles.
> 
> Que chaque participant qui a payé puisse inviter un noob gratuitement sur une journée du festival, pour lui faire découvrir, ça ce serait utile et ça ramènerait du monde ! :)
> 
> Et chuis d’accord avec le côté « viande fraiche ! lol »
> Déjà qu’en events hackers en tant que femmes on se coltine parfois des gros lourds, mais si en + l’organisation encourage cette idée de la fille qui est là pour décorer/accompagner/servir le thé/divertir…

<span></span>
> **Le 24 juin 2012 à 19 h 45 min, Aa a dit :**
> 
> J’ai un peu bondi en lisant cette histoire d’entrée gratuite…
> 
> En fait ce qu’il faudrait savoir c’est la motivation derrière cette opération : est-ce que l’idée était d’avoir une ambiance « festive » en même temps que le hacking et alors on est dans la même mentalité que pour les boîtes de nuit, avoir « de la chair fraîche » ou, moins méchamment, varier un peu les publics ? Pas très classe mais compréhensible ;
> 
> Ou est-ce que l’idée était d’avoir plus de femmes participantes, de hackeuses ? Alors c’est très maladroit…
> 
> C’est un problème récurrent dans le milieu geek : on est très peu de femmes, vraiment peu. Mais il y a aussi très peu de noirs, très peu de handicapés, etc. En fait c’est juste un milieu très homogène, très « entre-soi », alors même que pris isolément les geeks ne sont pas machos ni rien, ils construisent juste une ambiance « de mecs » quand ils sont ensemble, et ça devient parfois rébarbatif pour des nanas, cercle vicieux…
> 
> Parmi les filles que je connais dans ce milieu (et selon mes opinions personnelles), en général on n’a pas envie d’être traitées autrement, pas envie d’être mises en avant, avantagées ou autre, on a juste envie d’être là *naturellement*, que ça semble normal à tout le monde… C’est très agaçant aussi par certains côtés d’être traitées différemment parce que femmes, ça enferme aussi dans une différence.
> 
> Il est très compliqué de trouver une bonne solution pour faire venir plus de filles dans ce genre d’évènements (ou de communautés), parce qu’en réalité le problème vient de bien plus loin, en premier lieu sans doute du peu de filles dans l’informatique en général. Donc je ne jetterai pas la pierre trop fort (mais je la jette quand même ^^) aux organisateurs si ils ont cru bien faire.
> 
> Simplement pour une prochaine fois, juste dire « venez accompagné, votre accompagnant a son entrée gratuite », c’est bien moins discriminant (si je suis une nana codeuse et mon copain non, si je suis homo, si je viens juste avec un pote qui découvre etc.). Peut être que dans les faits ça va juste faire venir plus de filles, pourquoi pas. Mais au moins ça ne sera pas discriminant…
> 
> Ah et sinon toute une équipe d’assoces libristes organise un « apéro diversité » en octobre, on va essayer de justement favoriser la diversité sous toutes ses formes (pas que les femmes) dans notre milieu de geeks, avec un esprit positif et ouvert : venez ! http://wiki.april.org/images/8/8e/Affiche-apero_diversite2012.svg :-)
> > **Le 24 juin 2012 à 19 h 52 min, [Gordon](https://www.gordon.re/) a dit :**
> > 
> > Parfaitement d’accord avec toi, merci de ta contribution.
> > 
> > Juste un troll sur la fin : la diversité, c’est d’habiter à Paris (ou suffisamment près) ? ;)
> > > **Le 24 juin 2012 à 20 h 30 min, Aa a dit :**
> > > 
> > > Rahhh dis pas ça, tu parles à une provinciale pure souche égarée malencontreusement à Paris… :D
> > > 
> > > L’idée c’est de commencer, mais l’idée de sortir de Paris est là aussi, bien sûr !

<span></span>
> **Le 24 juin 2012 à 20 h 28 min, [Mitsu](http://www.suumitsu.eu/) a dit :**
> 
> La génétique a doté les hommes d’une plus grande capacité de réflexion et les femmes d’une plus grande capacité émotionnelle. Faut croire que l’évolution a trouvé ce qui est le mieux pour l’espèce (et ça peut changer à terme), mais quoi qu’il en soit: il y a une réelle différence physique et mentale entre les hommes et les femmes. C’est pas du sexisme, c’est un constat froidement scientifique. Ce qu’il faut impérativement souligner, c’est que c’est des TENDANCES: chacun est unique. Ça on peut pas le changer (et il ne faut pas le changer), libre à chacun d’être ce qu’il est et de vivre comme il l’entend.
> 
> Entre ce qu’on veut être et ce qu’on est il y a une différence qui peut être minime comme immense. Mon avis est que la société ne doit pas imposer une catégorisation aux individus, imposer une classe, une « place » pour chacun, un « modèle » individuel, familial ou sociétal. La loi sur la parité hommes/femmes en politique ? Profondément sexiste, rétrograde, discriminant, inégalitaire et dégradant, voilà ce que j’en dis. Et ces entrées gratuites pour les filles ? Pareil.
> 
> Présumer que les filles SANS DISTINCTION ne peuvent pas être intéressées par la NdH (parce que « c’est pas un truc de filles » ou parce qu’elles devraient juste être bonnes à accompagner leur geek de copain ou régaler les yeux des autres, peu importe les raisons) c’est une erreur immonde. Que je sache la NdH n’est pas un immense « speed dating » auquel il faut apporter de la chair fraîche (ou des animaux à l’abattoir, c’est selon). La NdH s’adresse à tou(te)s les intéressé(e)s qu’ils/elles soient accompagné(e)s ou pas, la discrimination XX / XY n’a pas lieu d’être. Pire: je vois ce « cadeau » fait aux filles comme une grosse insulte envers les filles, l’illustration que la catégorisation est assumée et imposée, pour des thèmes qui, rappelons-le, relèvent beaucoup plus des passions de chacun (et leur partage) que de la volonté de perpétuer l’espèce !!
> 
> (et d’ailleurs, qui a dit que la NdH se limitait aux homo sapiens ? les autres êtres vivants aussi sont dotés d’intelligence !)
> 
> Bref, que l’entrée soit à 20 € pour tous. Y compris pour les poneys :)
> 
> ps: … et pourquoi elle est payante, l’entrée ? ô_ô Le partage du savoir et des connaissances doit il être limité à ceux qui ont des euros, de la monnaie, voire tout simplement de la richesse matérielle ? J’aime pas ça, moi…
> > **Le 24 juin 2012 à 20 h 39 min, [Gordon](https://www.gordon.re/) a dit :**
> > 
> > Pour ta dernière question, c’est tout bêtement que louer une salle de Disneyland, ça coûte des brouzoufs. Et que HZV ne peut pas se permettre de payer ça chaque année tout seul.
> > 
> > Sinon, évidemment qu’il y a des différences entre un homme et une femme. Et il y en a aussi entre nous deux, peut-être même plus. So ? On est tous des lolpixels avec une interface faite en viande, après, chacun se customise comme il veut. Effectivement, il n’y a pas lieu de catégoriser pour autant.
> > > **Le 24 juin 2012 à 21 h 01 min, [Mitsu](http://www.suumitsu.eu/) a dit :**
> > > 
> > > (le sujet serait encore plus intéressant à étudier si l’on pouvait changer d’apparence physique et/ou de sexe génétique instantanément et à volonté, ainsi que le développement de la robotique adaptative, on en reparle dans quelques décennies ?)
> > > 
> > > Au sujet de la salle, des frais de location des lieux, il me semble mieux que la charge soit à ceux qui donnent et pas à ceux qui reçoivent. En clair: que les frais soient assurés par les exposants et pas du tout par les visiteurs. Au risque de se retrouver avec une salle sans exposants, mais ça à la limite c’est bien: les soirées hack dans les caves de chacun c’est la classe, c’est plus « dans l’esprit hack ». Promis si je descends sur Nice, j’apporte ma carte contrôleur SATA-miniUSB soudée/scotchée maison, ça va plaire à Gordon certainement :D
> > > 
> > > L’internet c’est magnifique parce que c’est anonyme/pseudonyme. Si je vous dis que je suis une fille: voilà, je suis une fille. Si je vous dis que je suis un garçon: voilà, je suis un garçon. Si je vous dis que je suis un renard panda roux à tricorne « tape & bones » avec bandeau pirate sur l’oeil gauche, .. bah voilà, considérez-moi ainsi car c’est le sens de mon pseudonymat actuel :) Chacun se juge non pas sur son apparence physique ou ses gênes, mais bien sur ce qui a été réalisé, pensé, communiqué, présenté au monde entier. Si l’internet doit sur ce point préfigurer la société de demain, j’adhère sans réserves dès maintenant ;)
> > > > **Le 24 juin 2012 à 21 h 07 min, [Gordon](https://www.gordon.re/) a dit :**
> > > > 
> > > > C’est le choix qui a été fait par les orgas de la NdH. D’autres en ont fait d’autres : PSES est financé par ses sponsors, le THSF est financé collaborativement. On peut ne pas être content, mais dans ce cas, autant aller dans un autre ou faire le sien :)
> > > > > **Le 25 juin 2012 à 20 h 53 min, [Ju](http://seteici.ondule.fr/) a dit :**
> > > > > 
> > > > > Ca vaudrait le coup de savoir cmbien coute la location ici http://www.disneylandparis-business.com/fr/conferences_reunions/centre_de_congres_du_disneys_hotel_newyork mais qqch me dit qu’on est sur des budgets faramineux par rapport au THSF

<span></span>
> **Le 24 juin 2012 à 21 h 26 min, [Changaco](http://changaco.net/) a dit :**
> 
> Je ne peux que plussoyer ce billet égalitariste.

<span></span>
> **Le 30 juin 2012 à 19 h 48 min, [tr00ps](http://www.hackerzvoice.net/) a dit :**
> 
> Bonjour.
> 
> Notre but a donner des acces gratuit aux femmes n est en aucun ca de ramener de la chair fraiche. Le pourcentage de femme reste relativement faible malgre une evolution depuis 2 ans.
> 
> Nous faisons la gratuite des femmes pour plusieurs raisons :
> 
> Que les conjointes copines peuvent un peut plus comprendre la passion de son partenaire sans que cela ne soit trop couteux
> Tanter de rendre le monde du hacking plus mixe.
> 
> Nous sommes ouvert a tout dialogues pour expliquer notre point de vue.
> 
> Notre but est une transmission d information et un partage du savoir pour tous. Femme homme jeune vieux etc…. un enfant n aurais pas payer non plus.
> 
> Entree gratuite pour les femmes n est pas forcememnt du sexisme.

<span></span>
> **Le 3 septembre 2012 à 14 h 39 min, Rouzz a dit :**
> 
> Salut,
> 
> Comme beaucoup de post dans ce genre je suis d’accord sur le fond.
> Ce qui me dérange c’est que comme les autres tu ne vois que le coté « la femme est la victime ».
> 
> Je suis désolé mais pour moi ce genre de choses est aussi insultant et dérangeant pour l’homme que pour la femme. Cela insinue que les hommes qui viendraient avec leur copine s’en servent de trophée ou que forcément si y’a plus de fille ça attire les mecs (tu sais, ces pervers en manque qui ne cherche qu’à voir des paires de seins).
> 
> Et pourquoi en boîte de nuit on part du principe que c’est le mec qui vient pour pécho ? Ca marche dans les deux sens aussi, donc c’est pas plus normal qu’il y ait ce genre d’offres.

