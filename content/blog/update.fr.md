Title: Update
Permalink: update
Date: 2011-09-26
Modified: 2011-09-26
Tags: mise à jour
Category: Blog

Vous vous trouvez aujourd’hui sur la nouvelle version du Kikooblog du Poney. Comme je suis un profond fanatique du référencement web, j’ai bien pris la peine de changer mon nom de domaine afin de dissuader d’éventuels lecteurs désireux de me suivre. Nouveau nom de domaine, donc, mais également nouvelle identité globale, car oui, Gordon c’est nettement plus rigolo que Gordontesos, dans le sens où ça occupera un peu plus longtemps les stalkers de tout poil.

<!-- cut -->

Mais au-delà de ça, c’est également une profonde refonte graphique, réalisée avec mes petits doigts barbus et qui, je l’espère, vous rendra la lecture agréable. Je suis parti du dernier template WordPress par défaut, qui utilise donc les derniers trucs rigolos implémentés dans le moteur de blog. Notamment en ce qui concerne les commentaires. Ceux d’entre vous qui ont commencé par lire le code HTML de la page avant cet article auront également remarqué le passage en HTML5, bien plus sémantique, et permettant je l’espère de ne jamais avoir à embedder un vieux lecteur Flash. De plus, et en adéquation avec mes valeurs, j’ai tenu à préciser que l’intégralité des contenus de ce blog étaient sous licence libre CC-BY-SA. Par ailleurs, en grand amoureux de la décentralisation et du respect de la vie privée, vous ne trouverez ici absolument aucun appel vers un script externe, ni aucune liaison avec un service tiers : aucun bouton de partage intrusif, donc. Je me limite à un bouton Flattr statique (sans appel externe), qui, bien qu’étant un système centralisé, me semble intéressant. Pour en apprendre plus à ce sujet, je ne peux que vous conseiller de lire ce qu’en dit Paul Da Silva, porte-parole officieux de Flattr en France. Et bien sûr, il est strictement hors de question de traquer votre comportement ou vos visites. Pas de Google Analytics, donc, mais aucun autre système de statistiques. Je ne saurai pas qui me visite, et je m’en porterai pas plus mal.

Ha, par contre, je vais mettre 5 bandeaux de pub flash, parce qu’il faut bien reconnaître que le but principal de ce blog est tout de même la rentabilisation.

Avant que j’oublie, je vous signale également la possibilité de naviguer sur ce blog (ainsi que sur l’intégralité des services que je serai amené à proposer – ce qui comprend les sites hébergés – en HTTPS, tout simplement en indiquant le protocole souhaité. Bien évidemment, le certificat n’est pas valide, car signé par moi-même, mais il s’agit à mon sens d’un défaut de conception du PKI sur lequel je reviendrai ultérieurement. Si vous souhaitez faire confiance à mon « autorité », vous pouvez utiliser ce certificat. Évidemment, faire ceci vous force à croire sur parole tout certificat que je pourrais générer; vous êtes prévenus.

En ce qui concerne enfin le contenu de ce blog, il sera vraisemblablement plus sérieux que l’ancien. Qui était plus sérieux que l’ancien, bref, en fait, rien ne change vraiment. Mais, histoire de vous teaser un peu, sachez qu’une poignée de gros articles est en préparation.

