Title: « Je ne suis pas féministe, je suis égalitariste »
Permalink: je-ne-suis-pas-feministe-je-suis-egalitaire
Category: Féminisme
Date: 2013-02-25
Modified: 2013-02-25
Tags: feminisme, masculinisme

Depuis quelques temps que je m’implique activement dans les débats sur le
féminisme, il y a cet argument, qui revient souvent, et qui est non seulement
inutile, mais surtout contre-productif. Je m’explique.

Lorsqu’on défend les valeurs féministes, par exemple en dénonçant un
comportement sexiste, on a tendance à heurter la sensibilité des hommes, qui ne
comprennent pas le problème. Se sentant attaqués, ils cherchent nativement à
discréditer les attaques. Lorsqu’il s’agit de défenseurs de l’égalité, il est
difficile de trouver des arguments cohérents. Alors ils attaquent la forme, à
commencer par le nom « féminisme ».

<!-- cut -->

### « Je suis antiféministe ».

J’ai eu envie de mettre une très grosse paire de claques à l’auteur de cette
phrase, que je considère comme un ami, et qui est très engagé dans les mêmes
valeurs que moi : la défense des libertés, l’égalité, le partage. Se déclarer
anti-féministe, ce n’est ni plus ni moins que de se déclarer contre l’égalité
des genres. Parce que le féminisme n’a **jamais** été la lutte contre les
hommes. Mais le fait est qu’avec des réactions spontanées comme celle-ci, il est
vrai qu’on a rarement envie de faire un câlin à leur auteur.

Pourquoi se déclarer « antiféminisme », en fait ? Parce qu’on croit qu’il ne
faut pas favoriser les femmes, et que la discrimination positive c’est mal ? Je
suis assez d’accord, mais on ne parle pas de discrimination positive, ici.

### Discrimination ?

Le féminisme, tel que je le vois (et tel qu’il est vu par celles et ceux qui
partagent ces valeurs), c’est la lutte pour l’**égalité** des genres. Et,
accrochez-vous bien, ça marche dans les deux sens. Les petites habitudes ancrées
dans la société, comme celle qui veut qu’on paie le restaurant à une femme, sont
aussi du sexisme, et sont aussi mal vues. Notez tout de même l’échelle : ce que
les femmes vivent à cause du sexisme, c’est par exemple la peur constante
d’être agressées lorsqu’elles se retrouvent dans la rue, tandis que ce que les
hommes redoutent le plus, c’est de « devoir » payer le restaurant. Alors on
combat toutes les formes de discrimination basées sur le genre, mais soyez
honnêtes : ce sont les femmes, en immense majorité, qui en sont victimes.

Pourquoi on parle de *féminisme*, et pas d’*égalitarisme* ? Pour cette raison.
Parce qu’on ne se voile pas la face, qu’on admet que le problème touche
essentiellement les femmes, et parce qu’il ne faut pas l’oublier ni le nier. Ça
s’appelle les « male tears », et c’est une réponse fort courante, quand on
évoque des problèmes de sexisme : un homme répond « oui mais regardez-moi, j’ai
des problèmes moi aussi », dans le but de détourner l’attention du problème
initial (souvent en se cachant derrière un problème tout à fait minime).
Demander à ce qu’on parle d’« égalitarisme » à la place de « féminisme », c’est
la même logique : on détourne le regard du fait que ce sont quasi-exclusivement
les femmes qui en sont victimes, on exige, en notre qualité de mâle, de profiter
aussi de ce combat (alors que c’est déjà le cas), mais surtout, qu’on enlève
cette référence exclusive aux femmes !

Alors on se dit « ho, faut pas le prendre comme ça, je cherche pas à mal, je
veux juste qu’on soit précis sur le terme ». Mais l’effet concret est que l’on
attaque les gens qui parlent de ces problèmes, qu’on les discrédite. Donc pour
le coup, oui c’est antiféministe, parce qu’on utilise des arguments de forme
pour dénigrer les arguments féministes. On se place contre eux, parce qu’on
n’aime pas le nom. C’est ridicule, et c’est totalement contraire aux valeurs que
la plupart de ceux que je connais défendent.

### Féminisme VS masculinisme

Si vous avez suivi, on peut résumer bêtement « féminisme = lutte pour
l’égalité ». Mais alors, quid du masculinisme, dont on entend parler ces
derniers temps ? S’agit-il du pendant masculin de la lutte pour l’égalité ?
C’est tout le contraire, en fait.

Pourquoi ne serait-ce pas le cas ? Parce que dans nos sociétés lourdement
sexistes, l’homme est **toujours** en haut. C’est lui le privilégié, c’est lui
qui a mis en place ce sexisme pour rester à la « bonne » place. Alors, lutter
pour que les femmes soient mieux considérées, et appeler ça le masculinisme ? Ça
n’a pas de sens. Par contre, lutter pour conserver, et même augmenter ses
privilèges, ça ce sont les valeurs du masculinisme. Chercher à faire taire la
lutte féministe, parce qu’on ne voudrait surtout pas partager le trône : « on
n’est pas bien, entre couilles ? ». Le masculinisme, c’est la lutte pour le
retour de la femme à la cuisine, pour revenir sur leurs droits. En somme, le
culte du patriarcat.

### Les privilèges masculins

Le simple fait d’être un homme a beaucoup d’implications en termes de sexisme.
Vous pouvez être fervent féministe, il est important de ne pas ignorer cet état
de fait : la société est construite sur des valeurs sexistes, et les hommes sont
privilégiés. Par exemple, si vous êtes un homme :

- pensez-vous régulièrement à la façon dont vous réagiriez si vous étiez agressé
dans la rue ?
- vous fait-on des remarques si votre tenue vestimentaire est trop provocante,
  ou même pas assez ?
- vous traite-t-on spontanément comme responsable si vous vous faites agresser ?
  (exemples : « tu es sûr que tu ne voulais pas ? », « c’est normal, vu comment
  tu étais habillé », « fallait pas sortir tout seul »…)
- considère-t-on que vous devez être protégé en permanence par quelqu’un du
  genre opposé, car vous ne savez pas vous défendre ?

Si vous avez répondu « oui » à ces affirmations, soit vous êtes d’une
affligeante mauvaise foi, soit votre style de vie vous a certainement fait
comprendre que, dans la majorité des cas, être un homme était mieux perçu en
société. Les hommes ont le privilège de ne pas avoir peur en permanence dans la
rue (une rue standard d’une ville standard d’un pays en paix, disons). **Les
hommes ont le privilège de ne même pas être conscients de ce privilège**. Ce
n’est pas un reproche, puisque ce n’est pas la faute des hommes, mais de la
société. Seulement, il faut le savoir. Il faut savoir ce que peut être la vie
d’une femme aujourd’hui. Je suis prêt à parier que ça fera relativiser
certains qui trouvent que « les féministes en font un peu trop ».


