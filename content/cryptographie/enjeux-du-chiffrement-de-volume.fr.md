Title: Enjeux du chiffrement de volume
Permalink: enjeux-du-chiffrement-de-volume
Date: 2013-02-13
Modified: 2013-02-13
Tags: luks, ecb, cbc, ctr
Category: Cryptographie
Status: draft

J’ai déjà plusieurs fois évoqué le [chiffrement intégral de disque
dur](/category/sysadmin.html) ici, mais sans m’attarder sur les implications de
ce type de chiffrement. Car il y a quelques particularités par rapport à du
chiffrement de message simple :

1. en chiffrant tout son système, on va naturellement chiffrer des données qui,
   loin d’être secrètes, sont parfaitement prévisibles, car communes à de
   nombreux systèmes d’exploitation (par exemple, les binaires système). Pour
   peu qu’on tombe sur une installation relativement par défaut d’une
   distribution connue, on sait à peu près ce qui est installé, et où. On
   dispose donc du texte clair de certains secteurs. Il faut savoir que les
   attaques par texte connu sont généralement les plus efficaces en
   cryptanalyse.
2. le simple fait qu’on chiffre un système de fichiers indique certaines
   informations prédictibles, par exemple dans les entêtes du système de
   fichier. C’est potentiellement plus difficile de savoir précisément ce qui
   est écrit, et où, mais ça fournit une information sur une partie de texte en
   clair.
3. On gère de grandes quantités de données chiffrées (potentiellement plusieurs
   centaines de Gio), et on doit pouvoir accéder rapidement à une partie ou une
   autre du volume. Cela signifie que, contrairement à un message simple, on ne
   peut pas déchiffrer puis rechiffrer l’intégralité du volume quand on veut y
   accéder.

Les deux premiers points posent des problèmes de sécurité. En effet, si on peut
deviner certains blocs en clair, il devient possible de faire de l’analyse
dessus. Le troisime point est lié aux performances (il est inutile d’user d’une
sécurité élevée s’il devient impossible de lire ses données dans des conditions
normales).
