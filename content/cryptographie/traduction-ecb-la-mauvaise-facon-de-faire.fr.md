Title: [Traduction] ECB : La mauvaise façon de faire
Permalink: traduction-ecb-la-mauvaise-facon-de-faire
Date: 2012-12-17
Modified: 2012-12-17
Tags: traduction, rogdham, cryptographie, ecb
Category: Cryptographie

Voici une traduction de [Rogdham](http://www.rogdham.net/2012/11/24/ecb-youre-doing-it-wrong.en), un hacker que j’ai eu le plaisir de rencontrer au [Nicelab](https://nicelab.org). Ce billet est une explication très claire des problèmes de conception du mode <acronym title="Electronic CodeBook">ECB</acronym> appliqué aux algorithmes de chiffrement.

<!-- cut -->

### ECB : La mauvaise façon de faire

#### 1 — Electronic CodeBook

Le chiffrement par blocs (tels que l’<acronym title="Advanced Encryption Standard">AES</acronym>) est une bonne méthode pour chiffrer un bloc (par exemple de 16 octets) de données en utilisant un algorithme qui a été démontré (quelle qu’en soit la méthode) comme sûr.

Le problème est que les messages que nous voulons envoyer chiffrés sont généralement plus longs que 16 octets. D’où le besoin de passer d’un algorithme par blocs à un algorithme par flux.

Pour ce faire, il existe différents modes d’opération. Parmi eux, ECB est le plus simple : on considère le message comme une suite consécutive de blocs, et on applique simplement le chiffrement à chaque bloc séparément. Cependant, ce mode est connu pour ne pas être sûr, étant donné qu’il est sujet aux analyses fréquentielles.

<div class="center"><img src="/images/ecb/tux_ecb.jpg" alt="Tux chiffré en mode ECB : nous pouvons toujours reconnaître qu’il s’agit de la mascotte de Linux" /></div>

L’image ci-dessus a été chiffrée en utilisant le mode ECB. Nous avons perdu des détails (comme les couleurs utilisées), mais il est assez clair que l’image originale était Tux, la mascotte de Linux.

Bon, cet exemple avait pour but de démontrer la faiblesse de ECB. Comment un attaquant s’y prendrait-il pour exploiter un algorithme de chiffrement *sûr* qui utiliserait le mode ECB ?

#### 2 — Contexte

Comme vous l’avez peut-être lu dans [mon précédent article](http://r.rogdham.net/15), j’ai participé au [PoliCTF](http://polictf.it/), qui était une compétition de sécurité en <acronym title="Capture The Flag">CTF</acronym>. Le challenge B1N 500 consistait à récupérer un fichier depuis un programme binaire.

La partie du programme qui nous intéressera pour le reste de cet article fonctionne comme tel :

1. Il demande une clé à l’utilisateur.
2. Il déchiffre des données avec AES en mode ECB en utilisant cette clé.
3. Il enregistre les données dans un fichier appelé 0.bmp.

Comment vous y prendriez-vous ? Si vous voulez essayer, [voici le fichier <acronym title="BitMaP">BMP</acronym> obtenu lorsque l’on donne une clé vide au binaire](/images/ecb/polictf_ecb_0.bmp). Bien sûr, il ne s’agit pas d’une image BMP valide.

J’ai réussi à obtenir le token lors de ce CTF, mais ça n’était que la première étape (sur 3) du challenge B1n 500. Malheureusement, nous n’avons pas été capables d’aller plus loin faute de temps, donc vous ne trouverez pas d’exploit complet ici.

#### 3 — Exploit

L’exploit commence avec de l’analyse fréquentielle.

Étant donné qu’on sait qu’AES est utilisé, on peut assumer que les blocs font 16 octets. Donc on découpe le fichier en blocs, et on compte la répartition des différents blocs.

<div class="center"><img src="/images/ecb/polictf_ecb_frequencies.png" alt="Analyse fréquentielle" /></div>

Comme le montre le graphique ci-dessus, un même bloc représente 90% de la taille du fichier, le second 6,8%, et les autres sont à moins de 1%.

L’idée suivante est simple : remplaçons les blocs les plus fréquents par des pixels blancs, et les second plus fréquents par des pixels noirs, et laissons de côté les autres.

Mais nous avons un problème : quelle est la taille de l’image ? La méthode par tâtonnage est appropriée dans ce cas. Essayez différentes valeurs, et vous obtenez finalement une image valide !

<div class="center"><img src="/images/ecb/polictf_ecb_first_result.png" alt="Premier résultat" /></div>

C’est loin d’être parfait, pour deux raisons :

* Nous avions oublié que le format BMP liste les pixels de bas en haut et de gauche à droite, alors l’image doit être retournée.
* Il y a un certain décalage à faire, étant donné que nous ne connaissons pas la taille des en-têtes du fichier (j’ai également retiré un certain nombre de pixels blancs avant et après cette image).

Étant donné qu’il s’agissait d’un CTF, j’étais pressé par le temps, alors j’ai simplement ouvert mon éditeur d’images favori, effectué une transformation miroir et lu le token : <kbd>Trolld</kbd>. Mission accomplie.

Ceci dit, pour aller plus loin, nous devrions envisager ceci :

* Partir du principe que le header fait 54 octets, ce qui aurait résolu le problème de décalage.
* Utiliser plus que les 2 plus fréquents blocs.

Ce qui donne le résultat suivant :

<div class="center"><img src="/images/ecb/polictf_ecb_second_result.png" alt="Second résultat" /></div>

#### 4 — Conclusion

Le mode ECB n’est pas sûr. Nous le savions déjà, mais cela a été particulièrement facile de savoir à quoi l’image ressemblait sans avoir la moindre idée de la clé.

Bien sûr, cela a fonctionné parce que le format BMP n’est pas compressé, ou quoi que ce soit. La compression est un moyen d’agrandir l’entropie des données. Mais si l’attaquant a suffisamment de données, l’analyse fréquentielle fonctionnera toujours. Ceci dit, si vous augmentez l’entropie de façon à ce que vos données aient l’air aléatoires, vous serez en sécurité. Devinez quoi ? C’est exactement ce que fait le chiffrement.

Et il est inutile d’utiliser un mauvais mode de chiffrement par-dessus un bon. Contentez-vous d’en utiliser un meilleur.
