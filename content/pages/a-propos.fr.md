Title: À Propos
Permalink: a-propos

Gordon, geek libriste hacktiviste, tout pour plaire.

Je suis une sorte de super-héros, pissant du code le jour, pissant du code libre
la nuit, et passant le reste de son temps à se taper des franches déconnades
avec des serveurs (pas ceux des bars, hein).

Gus dans [un garage](http://laquadrature.net/), qui donne un coup de barbe de
temps en temps. [Libriste](http://april.org/) convaincu.
[Gentooïste](http://gentoo.org/) à temps plein, et (plus vraiment) fraîchement
[bépoète](http://bepo.fr/) parce qu’il faut bien passer le temps pendant la
compilation de tex-live (et puis ça réchauffe les doigts). J’aime bien donner
des coups de main à des gens en hébergeant ce dont ils ont besoin. Et je suis un
paranoïaque sécuritaire (mais pas la sécurité qu’on apporte en collant des
        caméras partout) profond. Il m’arrive d’ailleurs de traîner avec des
[gens](http://telecomix.org/) qui mettent du datalove
partout.

Avec tout ça, je trouve le temps de fonder et présider une [usine d’adresses
hypées](http://ndn-fai.fr/), rejeton du vénérable [FDN](http://fdn.fr/). Et
comme on voulait construire des modems libres, on a lancé le
[Nicelab](https://wiki.nicelab.org/), hackerspace qui sent bon le soleil, et
pour déconner, on en a fait [un second](https://rivierhack.org/) dans la foulée.
C’est comme ça qu’on rigole, dans le Sud.

Comme je suis plutôt old school, si vous souhaitez me contacter, vous aurez
plus de chances de me joindre par mail que sur quelque réseau social merdique.
Il est assez facile de trouver une adresse mail sur laquelle je suis
susceptible de répondre, au hasard sur le compte « contact » de ce nom de
domaine. Et utilisez cette [clé GPG](/files/damien.asc) pour
chiffrer le message, soyez gentil. Voici l’empreinte de la clé, histoire de
vérifier :

    1633 367B 013F 9F61 B75E BE62 23B8 17F1 8593 65CC
