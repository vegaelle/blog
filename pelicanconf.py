#!/usr/bin/env python

AUTHOR = "Gordon"
SITENAME = "blogordon-5.0.0"
SITESUBTITLE = "Le dump de mon cerveau"
# SITEURL = 'https://gordon.re'
SITEURL = 'http://localhost:8000'

USE_FOLDER_AS_CATEGORY = True

# FILENAME_METADATA = r'(?P<slug>[^\.]+?)(.(?P<lang>[a-z]{2}))?.md'
FILENAME_METADATA = r'(?P<slug>[^.]+)(\.(?P<lang>[a-z]{2,3}))?'

ARTICLE_URL = '{category}/{permalink}.html'
ARTICLE_SAVE_AS = '{category}/{permalink}.html'

ARTICLE_LANG_URL = '{category}/{permalink}.html'
ARTICLE_LANG_SAVE_AS = '{category}/{permalink}.html'

PAGE_URL = 'pages/{permalink}.html'
PAGE_SAVE_AS = 'pages/{permalink}.html'

PAGE_LANG_URL = 'pages/{permalink}.html'
PAGE_LANG_SAVE_AS = 'pages/{permalink}.html'

# STATIC_URL = 'static/{slug}.html'
# STATIC_SAVE_AS = 'static/{slug}.html'
STATIC_LANG_URL = '{path}'
STATIC_LANG_SAVE_AS = '{path}'

MENUITEMS = [('Accueil', '/')]

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'

DEFAULT_DATE_FORMAT = '%A %d %B %Y'

DATE_FORMATS = {
    'en': '%a, %d %b %Y',
    'fr': '%A %d %B %Y',
}

languages_lookup = {
    'en': 'english',
    'fr': 'français',
    }


def lookup_lang_name(lang_code):
    if lang_code is None:
        lang_code = DEFAULT_LANG
    return languages_lookup[lang_code]


JINJA_FILTERS = {
    'lookup_lang_name': lookup_lang_name,
}

DISPLAY_PAGES_ON_MENU = True

DISPLAY_CATEGORIES_ON_MENU = True

LOCALE = 'fr_FR.UTF-8'

OUTPUT_SOURCES_EXTENSION = '.md'

WITH_FUTURE_DATES = True

FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = 'feeds/{slug}.atom.xml'

# Blogroll
LINKS = (
    # ('<img src="/images/logos/ndn.png" alt="NDN" />',
    #  'Nice Data Network', 'http://ndn-fai.fr'),
    # ('<img src="/images/logos/nicelab.png" alt="Nicelab" />',
    #  'Nicelab', 'https://nicelab.org'),
    # ('<img src="/images/logos/logo_faimaison.png" alt="Faimaison" />',
    #  'Faimaison', 'http://faimaison.net'),
    # ('<img src="/images/logos/logo_naohack.png" alt="Naohack" />',
    #  'Naohack', 'http://naohack.org'),
    ('<img src="/images/logos/logo_fdn.png" alt="French Data Network" />',
     'French Data Network', 'http://www.fdn.fr'),
    ('<img src="/images/logos/logo_laquadrature.png" alt="La Quadrature du '
     'Net" />', 'La Quadrature du Net', 'https://laquadrature.net'),
    ('<img src="/images/logos/logo_telecomix.png" alt="Telecomix" />',
     'Telecomix', 'https://telecomix.org'),
    ('<img src="/images/logos/logo_april.png" alt="April" />',
     'April', 'https://www.april.org'),
    ('<img src="/images/logos/logo_eff.png" alt="Electronic Frontier '
     'Fundation" />', 'Electronic Frontier Fundation', 'https://www.eff.org'),
    ('<img src="/images/logos/logo_gentoo.png" alt="Gentoo" />',
     'Gentoo', 'http://www.gentoo.org'),
    ('<img src="/images/logos/logo_bepo.png" alt="Bépo" />',
     'Bépo', 'http://bepo.fr'),
)

# Social widget
SOCIAL = (('You can add links in your config file', '#'),
          ('Another social link', '#'),)

# Static paths will be copied under the same name
STATIC_PATHS = ['files', 'images']

DEFAULT_PAGINATION = 10
THEME = 'gordon'

OUTPUT_SOURCES = True

MARKDOWN = {'extensions': ['codehilite', 'extra']}

RELATIVE_URLS = True

TEMPLATE_PAGES = {'search.html': 'search.html'}

PLUGIN_PATHS = ['./pelican-plugins']
PLUGINS = ['i18n_subsites', 'sitemap', 'tipue_search', 'pelican-open_graph']

SITEMAP = {'format': 'xml'}

ISSO_SERVER = 'https://comments.gordon.re'

JINJA_ENVIRONMENT = {'extensions': ['jinja2.ext.i18n']}

I18N_GETTEXT_NEWSTYLE = True

I18N_SUBSITES = {
    'en': {
        'SITESUBTITLE': 'My brain’s dump',
        'LOCALE': 'en_US.UTF-8',
        'MENUITEMS': [('Index', '/')],
        'STATIC_PATHS': ['files', 'images'],
        }
    }
